<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180619_110935_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'title'=> $this->string(),
            'body'=> $this->text(),
            'category_id'=> $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }
}

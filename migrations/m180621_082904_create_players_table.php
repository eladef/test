<?php

use yii\db\Migration;

/**
 * Handles the creation of table `players`.
 */
class m180621_082904_create_players_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('players', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
            'team'=> $this->string(),
            'age'=> $this->integer(),
             'E-mail'=> $this->text(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('players');
    }
}


